/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daisy.mojo;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Daisy
 */
public class StringRev {
    public static void main(String[] args){
        
        Stack<String> stack=new Stack<>();
        
        System.out.print("Enter a string");
        Scanner input=new Scanner(System.in);
        String sentence=input.nextLine();
        
        String[] str=sentence.split(" ", 2);
        
        for (String s:str){
            stack.push(s);
        }
        
        for(int i=0; i<=2;i++){
            System.out.print(stack.pop());
        }
    }
}
