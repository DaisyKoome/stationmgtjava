/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package policestation;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
/**
 *
 * @author Daisy
 */
public class PoliceStationFile {
    String passFileName="police.txt";
    
    void createUsersFile(String policeStationName,String policeOfficerName, String incidentDate){
        //create bytes to write to file
        String oneLine = policeStationName+" "+policeOfficerName+" "+incidentDate+" "+"\n";
        byte data[] = oneLine.getBytes();
        try{
            File usersFile = new File(passFileName);
            if (!usersFile.exists()){
                    OutputStream outPassFile = new FileOutputStream(passFileName);
                    outPassFile.write(data,0,data.length);
            }else{
                OutputStream outPassFile = new FileOutputStream(passFileName,true);
                 outPassFile.write(data,0,data.length);
            }
        }catch(IOException e) {
         System.out.print("Exception: " +e.getMessage());
        
        }
    
    }
}
